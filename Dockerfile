FROM ghcr.io/thenativeweb/get-next-version:2.4.2 as get-next-version

FROM debian:bullseye-slim
COPY --from=get-next-version /action/get-next-version /bin/get-next-version
RUN apt-get update && apt-get install --no-install-recommends -y \
    git \
    curl \
    jq \
    ca-certificates
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*